const assignmentDate = '1/21/2019';

// Convert to a Date instance

let date = new Date( Date.parse('1/21/2019') );
console.log(date); // return Mon Jan 21 2019 00:00:00 GMT-0800 (Pacific Standard Time)
alert(date.toDateString());//return "Mon Jan 21 2019"

// Create dueDate which is 7 days after assignmentDate
const dueDate = new Date (date.setDate(date.getDate() + 7));
console.log(dueDate); // return Mon Jan 28 2019 00:00:00 GMT-0800 (Pacific Standard Time)

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log



// #1  
const timeStamp = dueDate.toDateString().split(' '); //return (4) ["Mon", "Jan", "28", "2019"]
	
    var day = timeStamp[1];
    var month = timeStamp[2];
    var year = timeStamp[3];

const dueDateMsg = 'Assignment due '+ month + ' ' + day +', '+ year;

console.log(dueDateMsg);

document.getElementById("datetime").innerHTML = month + ' ' + day +', '+ year;

// // #2

// function format(input) {
//     return new Date(input).toLocaleDateString('en-GB', {
//         month: 'long',
//         day: '2-digit'
//     });
// }

// document.getElementById("datetime").innerHTML = format(dueDate) +', ' + dueDate.getFullYear();
