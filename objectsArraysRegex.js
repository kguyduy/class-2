/* 
 * Create an object representation of yourself
 * should include firstName, lastName, 'favorite food'
 * should also include a mom object and a dad object with
 * the same properties as above
 */
let myFamily = { 
	firstName: 'Katrina', 
	lastName: 'Guyduy', 
	'favorite food': 'sushi',
	dad: {
		firstName: 'Jack', 
		lastName: 'Smith', 
		'favorite food': 'orange',
	},
	mom: {
		firstName: 'Maria', 
		lastName: 'Smith', 
		'favorite food': 'mango'
	}
};

 // console.log dad's firstName, mom's favorite food

console.log(myFamily.dad.firstName);
console.log(myFamily.mom['favorite food']);

/* 
 * Tic-Tac-Toe
 * Create an array to represent the tic-tac-toe board (see slide)
 */
let row1 = ['-','0','-'];
let row2 = ['-','X','0'];
let row3 = ['X','0','X'];

let board =[row1, row2, row3];


// After the array is created, 'O' takes the top right square.  Update.
board[0][2] = '0';


// Log the grid to the console.
// Hint: log each row separately.

console.log(board);

/*
 * Validate the email
 * You are given an email as string myEmail
 * make sure it is in correct email format.
 * Should be in this format, no whitespace:
 * 1 or more characters
 * @ sign
 * 1 or more characters
 * .
 * 1 or more characters
 */
const myEmail = 'foo@bar.baz'; // good email
const badEmail = 'badmail@gmail'; // bad email
